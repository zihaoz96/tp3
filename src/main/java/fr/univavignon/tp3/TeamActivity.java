package fr.univavignon.tp3;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;

import fr.univavignon.tp3.Match;
import fr.univavignon.tp3.Team;
import fr.univavignon.tp3.WebServiceUrl;

public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;


    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    public static ImageView imageBadge;
    private Team team;

    JSONResponseHandlerTeam jsonResponse = null;
    SportDbHelper sportDbHelper = new SportDbHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = (Team) getIntent().getParcelableExtra("teamSend");
        jsonResponse = new JSONResponseHandlerTeam(this.team);

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);
        imageBadge = (ImageView) findViewById(R.id.imageView);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO
                new AsyncTaskRunner().execute(team.getTeamBadge());
            }
        });

    }

    private final class AsyncTaskRunner extends AsyncTask<String, Void, Void>{


        @Override
        protected Void doInBackground(String... urls) {
            UpdateInfos updateInfos = new UpdateInfos(jsonResponse,team);
            updateInfos.execute();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            sportDbHelper.updateTeam(team);
            updateView();
        }
    }



    @Override
    public void onBackPressed() {
        //TODO : prepare result for the main activity
        Intent intent = new Intent(TeamActivity.this, MainActivity.class);
        startActivity(intent);
        super.onBackPressed();
    }

    private void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

        if (team.getIdTeam() != 0){
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/zihao/tp3/image/" + team.getIdTeam() + ".png");
            String filePath = file.getPath();
            Bitmap bitmap = BitmapFactory.decodeFile(filePath);
            imageBadge.setImageBitmap(bitmap);
        }
    }
}
